package bigdata.assign7;

import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;

public class RatingToTextUDF extends UDF {

	public Text evaluate(FloatWritable input) {		
		float rating = input.get();
		if (rating >= 1 && rating < 2)
			return new Text("Poor");
		else if (rating >= 2 && rating < 3)
			return new Text("Average");
		else if (rating >= 3 && rating < 4)
			return new Text("Good");
		else if (rating >= 4 && rating <= 5)
			return new Text("Excellent");
		else
			return new Text(“None”);
	}
}
